CREATE SCHEMA IF NOT EXISTS `calculator` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXISTS `calculator`.`session` (
  `session_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key of table',
  `remote_ip` VARCHAR(100) NOT NULL COMMENT 'Client IP',
  `cur_status` ENUM('I', 'A', 'C') NOT NULL DEFAULT 'I' COMMENT 'Current status of session: I=Initialized,A=Operand Added,C=Calculated ',
  `date_insert` DATETIME NOT NULL COMMENT 'Date and time of creation',
  PRIMARY KEY (`session_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `calculator`.`operand` (
  `operand_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key of table',
  `session_id` INT(11) NOT NULL COMMENT 'Foreign key of session table',
  `operand_value` DECIMAL(14,2) NOT NULL COMMENT 'Value of operand',
  `operation_id` INT(11) NULL DEFAULT NULL COMMENT 'Foreign key of operation table',
  `date_insert` DATETIME NOT NULL COMMENT 'Data and time when operand was inserted',
  PRIMARY KEY (`operand_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `calculator`.`operation` (
  `operation_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key of table',
  `session_id` INT(11) NOT NULL COMMENT 'Foreign key of session table',
  `oper_calc` ENUM('A', 'S', 'M', 'D', 'X') NOT NULL COMMENT 'Operation applied by client',
  `oper_result` DECIMAL(14,2) NOT NULL COMMENT 'Result of operation',
  `date_insert`DATETIME NOT NULL COMMENT 'Date and time of operation',
  PRIMARY KEY (`operation_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;