package com.jg.sixtera.service;

import com.jg.sixtera.entity.Operand;
import com.jg.sixtera.entity.Operation;
import com.jg.sixtera.entity.Session;
import com.jg.sixtera.repository.OperandRepository;
import com.jg.sixtera.repository.OperationRepository;
import com.jg.sixtera.repository.SessionRepository;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jigonzalez
 */
@Service
public class CalculatorService {

  Logger logger = LoggerFactory.getLogger(CalculatorService.class);

  @Autowired
  private SessionRepository sessionRepository;

  @Autowired
  private OperandRepository operandRepository;

  @Autowired
  private OperationRepository operationRepository;

  @Transactional
  public int createSession(String remoteIP) {
    logger.info("Creating new Session from " + remoteIP);
    Session session = new Session();
    session.setRemoteIp(remoteIP);
    session.setCurStatus("I");
    session.setDateInsert(new Date());
    sessionRepository.saveAndFlush(session);
    return session.getSessionId();
  }

  @Transactional
  public List<Operand> addOperand(Operand operand, String remoteIp) throws Exception {
    Integer sessionId = operand.getSessionId();
    logger.info("Adding operand for session " + sessionId);

    //CHECK IF SESSION ID WAS SENT
    if (sessionId == null || sessionId.equals(0)) {
      throw new Exception("session id is required");
    }
    Session session = sessionRepository.getOne(sessionId);

    //CHECK IF SESSION EXISTS
    if (session == null) {
      throw new Exception("Session has not been created");
    }
    //CHECK IF OPERAND IS SENT FROM SAME IP OF SESSION
    if (!session.getRemoteIp().equals(remoteIp)) {
      throw new Exception("Operand has not been sent from same ip of session");
    }
    operand.setDateInsert(new Date());
    //SAVE OPERAND RECORD
    logger.info("Creating operand with value " + operand.getOperandValue());
    operandRepository.saveAndFlush(operand);

    //UPDATE CUR STATUS OF SESSION
    session.setCurStatus("A");
    logger.info("Updating session to A");
    sessionRepository.saveAndFlush(session);
    return operandRepository.findBySessionWithoutOperation(sessionId);
  }

  @Transactional
  public Operation applyOperation(Operation operation, String remoteIp) throws Exception {
    Integer sessionId = operation.getSessionId();
    logger.info("Applying operation for session " + sessionId);
    //CHECK IF SESSION ID WAS sENT
    if (sessionId == null || sessionId.equals(0)) {
      throw new Exception("sessionId is required");
    }
    Session session = sessionRepository.getOne(sessionId);

    //CHECK IF SESSION EXISTS
    if (session == null) {
      throw new Exception("Session has not been created");
    }

    //CHECK IF OPERAND IS SENT FROM SAME IP OF SESSION
    if (!session.getRemoteIp().equals(remoteIp)) {
      throw new Exception("Operand has not been sent from same ip of session");
    }

    logger.info("Getting operands without operation");
    List<Operand> operandsWithoutOperation = operandRepository.findBySessionWithoutOperation(sessionId);

    //CHECK IF SESSION HAS OPERANDS
    if (operandsWithoutOperation == null || operandsWithoutOperation.size() < 2) {
      throw new Exception("Session needs at least 2 operands to calculate operation");
    }

    logger.info("operands without operation: " + operandsWithoutOperation.size());
    String operCalc = operation.getOperCalc();
    
    logger.info("Operation to appply : " + operCalc);

    if (operCalc == null || operCalc.isEmpty()) {
      throw new Exception("operCalc is required");
    }

    BigDecimal operResult = operandsWithoutOperation.get(0).getOperandValue();
    for (int i = 1; i < operandsWithoutOperation.size(); i++) {
      Operand operand = operandsWithoutOperation.get(i);
      switch (operCalc) {
        case "A":
          operResult = operResult.add(operand.getOperandValue());
          break;

        case "S":
          operResult = operResult.subtract(operand.getOperandValue());
          break;

        case "M":
          operResult = operResult.multiply(operand.getOperandValue());
          break;

        case "D":
          if (operand.getOperandValue().doubleValue() == 0) {
            throw new Exception("An operand is zero, operation not allowed");
          }
          operResult = operResult.divide(operand.getOperandValue());
          break;

        case "X":
          operResult = operResult.pow(operand.getOperandValue().intValue());
          break;

        default:
          throw new Exception("Operation not allowed");

      }
    }
    
    logger.info("Operation result : " + operResult);
    operation.setDateInsert(new Date());
    operation.setOperResult(operResult);
    //SAVE OPERAND RECORD
    logger.info("Saving operation");
    operation = operationRepository.saveAndFlush(operation);

    //UPDATE CUR STATUS OF SESSION
    session.setCurStatus("C");
    logger.info("Updating session to C");
    sessionRepository.saveAndFlush(session);
    logger.info("Updating operands with operation");
    for (Operand operand : operandsWithoutOperation) {
      operand.setOperationId(operation.getOperationId());
      
      operandRepository.saveAndFlush(operand);
    }
    
    logger.info("Creating new operand from result");
    Operand operand = new Operand();
    operand.setSessionId(sessionId);
    operand.setDateInsert(operation.getDateInsert());
    operand.setOperandValue(operResult);
    operandRepository.saveAndFlush(operand);
    return operation;
  }

  @Transactional
  public Session getSessionById(Integer sessionId) {
    logger.info("getSesion by ID: " + sessionId);
    Session session = sessionRepository.getOne(sessionId);
    session.setOperandsWithoutOperation(operandRepository.findBySessionWithoutOperation(sessionId));
    return session;
  }
}
