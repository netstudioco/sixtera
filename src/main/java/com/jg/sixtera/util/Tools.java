package com.jg.sixtera.util;

import java.sql.SQLException;
import javax.validation.ConstraintViolationException;
import org.springframework.orm.jpa.JpaSystemException;

/**
 *
 * @author jigonzalez
 */
public class Tools {

  public static void processError(Exception ex, RestResponse restResponse) {
    restResponse.setSuccess(false);
    String response = "";
    if (ex instanceof SQLException || ex instanceof JpaSystemException || ex instanceof ConstraintViolationException) {
      response = "Oops something is wrong with the data, please try again or contact your account manager to inform this issue";
    } else if (ex instanceof Exception) {
      response = ex.getLocalizedMessage();
    }
    restResponse.setResponse(response);
  }

}
