package com.jg.sixtera.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.jg.sixtera"})
@EntityScan("com.jg.sixtera.entity")
@EnableJpaRepositories("com.jg.sixtera.repository")
/**
 *
 * @author jigonzalez
 */
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
