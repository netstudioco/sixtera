
package com.jg.sixtera.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jigonzalez
 */
@Entity
@Table(name = "operand")

public class Operand implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "operand_id")
  private Integer operandId;

  @Basic(optional = false)
  @NotNull
  @Column(name = "session_id")
  private Integer sessionId;

  @Basic(optional = false)
  @NotNull
  @Column(name = "operand_value")
  private BigDecimal operandValue;

  @Column(name = "operation_id")
  private Integer operationId;

  @Basic(optional = false)
  @NotNull
  @Column(name = "date_insert")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateInsert;

  @ManyToOne(optional = false)
  @JoinColumn(name = "operation_id", insertable = false, updatable = false)
  private Operation operation;

  public Operand() {
  }

  public Operand(Integer operandId) {
    this.operandId = operandId;
  }

  public Operand(Integer operandId, Integer sessionId, BigDecimal operandValue, Date dateInsert) {
    this.operandId = operandId;
    this.sessionId = sessionId;
    this.operandValue = operandValue;
    this.dateInsert = dateInsert;
  }

  public Integer getOperandId() {
    return operandId;
  }

  public void setOperandId(Integer operandId) {
    this.operandId = operandId;
  }

  public Integer getSessionId() {
    return sessionId;
  }

  public void setSessionId(Integer sessionId) {
    this.sessionId = sessionId;
  }

  public BigDecimal getOperandValue() {
    return operandValue;
  }

  public void setOperandValue(BigDecimal operandValue) {
    this.operandValue = operandValue;
  }

  public Integer getOperationId() {
    return operationId;
  }

  public void setOperationId(Integer operationId) {
    this.operationId = operationId;
  }

  public Date getDateInsert() {
    return dateInsert;
  }

  public void setDateInsert(Date dateInsert) {
    this.dateInsert = dateInsert;
  }

  @Override
  public int hashCode() {
    return operandId;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Operand)) {
      return false;
    }
    Operand other = (Operand) object;
    if (this.operandId != other.operandId) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "com.jg.sixtera.test.entity.Operand[ operandId=" + operandId + " ]";
  }

}
