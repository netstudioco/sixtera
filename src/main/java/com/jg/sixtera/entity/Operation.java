/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jg.sixtera.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jigonzalez
 */
@Entity
@Table(name = "operation")

public class Operation implements Serializable {

  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "operation_id")
  private Integer operationId;
  
  @Basic(optional = false)
  @NotNull
  @Column(name = "session_id")
  private int sessionId;
  
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 2)
  @Column(name = "oper_calc")
  private String operCalc;
  
  // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
  @Basic(optional = false)
  @NotNull
  @Column(name = "oper_result")
  private BigDecimal operResult;
  
  @Basic(optional = false)
  @NotNull
  @Column(name = "date_insert")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateInsert;
  
  @ManyToOne(optional = false)
  @JoinColumn(name = "session_id", insertable = false, updatable = false)
  private Session session;
  
  @OneToMany(mappedBy = "operation", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Operand.class)
  private List<Operand> operands;

  public Operation() {
  }

  public Operation(Integer operationId) {
    this.operationId = operationId;
  }

  public Operation(Integer operationId, int sessionId, String operCalc, BigDecimal operResult, Date dateInsert) {
    this.operationId = operationId;
    this.sessionId = sessionId;
    this.operCalc = operCalc;
    this.operResult = operResult;
    this.dateInsert = dateInsert;
  }

  public Integer getOperationId() {
    return operationId;
  }

  public void setOperationId(Integer operationId) {
    this.operationId = operationId;
  }

  public int getSessionId() {
    return sessionId;
  }

  public void setSessionId(int sessionId) {
    this.sessionId = sessionId;
  }

  public String getOperCalc() {
    return operCalc;
  }

  public void setOperCalc(String operCalc) {
    this.operCalc = operCalc;
  }

  public BigDecimal getOperResult() {
    return operResult;
  }

  public void setOperResult(BigDecimal operResult) {
    this.operResult = operResult;
  }

  public Date getDateInsert() {
    return dateInsert;
  }

  public void setDateInsert(Date dateInsert) {
    this.dateInsert = dateInsert;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (operationId != null ? operationId.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Operation)) {
      return false;
    }
    Operation other = (Operation) object;
    if ((this.operationId == null && other.operationId != null) || (this.operationId != null && !this.operationId.equals(other.operationId))) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "com.jg.sixtera.test.entity.Operation[ operationId=" + operationId + " ]";
  }

  public List<Operand> getOperands() {
    return operands;
  }

  public void setOperands(List<Operand> operands) {
    this.operands = operands;
  }

}
