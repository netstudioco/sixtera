package com.jg.sixtera.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jigonzalez
 */
@Entity
@Table(name = "session")

public class Session implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "session_id")
  private Integer sessionId;

  @Basic(optional = false)
  @NotNull
  @Column(name = "remote_ip")
  private String remoteIp;

  @Basic(optional = true)
  @Column(name = "cur_status")
  private String curStatus;

  @Basic(optional = false)
  @NotNull
  @Column(name = "date_insert")
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateInsert;
  
  @Transient
  private List<Operand> operandsWithoutOperation;

  @OneToMany(mappedBy = "session", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Operation.class)
  private List<Operation> operations;

  public Session() {
  }

  public Session(Integer sessionId) {
    this.sessionId = sessionId;
  }

  public Session(Integer sessionId, String remoteIp, String curStatus, Date dateInsert) {
    this.sessionId = sessionId;
    this.remoteIp = remoteIp;
    this.curStatus = curStatus;
    this.dateInsert = dateInsert;
  }

  public Integer getSessionId() {
    return sessionId;
  }

  public void setSessionId(Integer sessionId) {
    this.sessionId = sessionId;
  }

  public String getRemoteIp() {
    return remoteIp;
  }

  public void setRemoteIp(String remoteIp) {
    this.remoteIp = remoteIp;
  }

  public String getCurStatus() {
    return curStatus;
  }

  public void setCurStatus(String curStatus) {
    this.curStatus = curStatus;
  }

  public Date getDateInsert() {
    return dateInsert;
  }

  public void setDateInsert(Date dateInsert) {
    this.dateInsert = dateInsert;
  }

  @Override
  public int hashCode() {
    return sessionId;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Session)) {
      return false;
    }
    Session other = (Session) object;
    if (this.sessionId != other.sessionId) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "com.jg.sixtera.test.entity.Session[ sessionId=" + sessionId + " ]";
  }

  public List<Operand> getOperandsWithoutOperation() {
    return operandsWithoutOperation;
  }

  public void setOperandsWithoutOperation(List<Operand> operandsWithoutOperation) {
    this.operandsWithoutOperation = operandsWithoutOperation;
  }

  public List<Operation> getOperations() {
    return operations;
  }

  public void setOperations(List<Operation> operations) {
    this.operations = operations;
  }
}
