package com.jg.sixtera.repository;

import com.jg.sixtera.entity.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jigonzalez
 */
@Repository
public interface OperationRepository extends JpaRepository<Operation, Integer> {
}
