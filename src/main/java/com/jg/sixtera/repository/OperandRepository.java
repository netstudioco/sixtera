package com.jg.sixtera.repository;

import com.jg.sixtera.entity.Operand;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jigonzalez
 */
@Repository
public interface OperandRepository extends JpaRepository<Operand, Integer> {

  @Query(value = "SELECT o FROM Operand o WHERE o.sessionId=(:sessionId) AND o.operationId IS NULL")
  List<Operand> findBySessionWithoutOperation(@Param("sessionId") Integer sessionId);
}
