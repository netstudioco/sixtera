package com.jg.sixtera.repository;

import com.jg.sixtera.entity.Session;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jigonzalez
 */

@Repository
public interface SessionRepository extends JpaRepository<Session, Integer> {
  
}