package com.jg.sixtera.controller;

import com.jg.sixtera.util.RestResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jigonzalez
 */
@RestController
@RequestMapping("/api/v1")
public class HelloWorldController {
  
  @GetMapping("/hello")
  public RestResponse hello() {
    RestResponse restResponse = new RestResponse();
    restResponse.setSuccess(true);
    restResponse.setResponse("Hello world");
    return restResponse;
  }
}
