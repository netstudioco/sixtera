package com.jg.sixtera.controller;

import com.jg.sixtera.entity.Operand;
import com.jg.sixtera.entity.Operation;
import com.jg.sixtera.service.CalculatorService;
import com.jg.sixtera.util.RestResponse;
import com.jg.sixtera.util.Tools;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jigonzalez
 */
@RestController
@RequestMapping("/api/v1")
public class CalculatorController {

  Logger logger = LoggerFactory.getLogger(CalculatorController.class);
  
  @Autowired
  CalculatorService calculatorService;

  @GetMapping("/session")
  public RestResponse createSession(HttpServletRequest request) {
    RestResponse restResponse = new RestResponse();
    try {
      int sessionId = calculatorService.createSession(request.getRemoteAddr());
      restResponse.setSuccess(true);
      restResponse.setResponse(sessionId);
    } catch (Exception ex) {
      logger.error(ex.getLocalizedMessage());
      Tools.processError(ex, restResponse);
    }
    return restResponse;
  }

  @PostMapping("/add-operand")
  public RestResponse addOperand(@RequestBody Operand operand, HttpServletRequest request) {

    RestResponse restResponse = new RestResponse();
    try {
      restResponse.setResponse(calculatorService.addOperand(operand, request.getRemoteAddr()));
      restResponse.setSuccess(true);
    } catch (Exception ex) {
      logger.error(ex.getLocalizedMessage());
      Tools.processError(ex, restResponse);
    }
    return restResponse;
  }

  @PostMapping("/apply-operation")
  public RestResponse applyOperation(@RequestBody Operation operation, HttpServletRequest request) {

    RestResponse restResponse = new RestResponse();
    try {
      restResponse.setResponse(calculatorService.applyOperation(operation, request.getRemoteAddr()));
      restResponse.setSuccess(true);
    } catch (Exception ex) {
      logger.error(ex.getLocalizedMessage());
      Tools.processError(ex, restResponse);
    }
    return restResponse;
  }

  @GetMapping("/session/{sessionId}")
  public RestResponse getSessionById(@PathVariable("sessionId") Integer sessionId) {

    RestResponse restResponse = new RestResponse();
    try {
      restResponse.setResponse(calculatorService.getSessionById(sessionId));
      restResponse.setSuccess(true);
    } catch (Exception ex) {
      logger.error(ex.getLocalizedMessage());
      Tools.processError(ex, restResponse);
    }
    return restResponse;
  }

}
