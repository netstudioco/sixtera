package com.jg.sixtera.test;

import java.util.LinkedHashMap;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorControllerTests {

  private RestClient restClient = new RestClient();
  int port = 8080;

  @Test
  public void testHelloService() throws Exception {
    RestResponse restResponse = restClient.createGetCall(port, "hello");
    assertNotNull(restResponse);
    assertTrue(restResponse.isSuccess());
    assertEquals("Hello world", restResponse.getResponse());
  }
  
  @Test
  public void testSessionService() throws Exception {
    RestResponse restResponse = restClient.createGetCall(port, "session");
    assertNotNull(restResponse);
    assertTrue(restResponse.isSuccess());
  }
  
  @Test
  public void testSessionAddOperand() throws Exception {
    RestResponse restResponse = restClient.createGetCall(port, "session");
    assertNotNull(restResponse);
    assertTrue(restResponse.isSuccess());
    Integer sessionId = Integer.valueOf(restResponse.getResponse().toString());
    LinkedHashMap<String, Object> params = new LinkedHashMap();
    params.put("sessionId", sessionId);
    params.put("operandValue", 2);
    restResponse = restClient.createPostCall(port, "add-operand", params);
    assertNotNull(restResponse);
    assertTrue(restResponse.isSuccess());
  }

}
