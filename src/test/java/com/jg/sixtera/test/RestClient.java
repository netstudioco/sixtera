package com.jg.sixtera.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author jimmy
 */
public class RestClient {

  public RestResponse createGetCall(int port, String service) throws UnirestException, IOException {
    HttpResponse<String> response = Unirest.get("http://localhost:" + port + "/api/v1/" + service)
            .header("accept-encoding", "gzip, deflate")
            .header("Connection", "keep-alive")
            .header("cache-control", "no-cache")
            .asString();
    String jsonResponse = response.getBody();
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(jsonResponse, RestResponse.class);
  }

  public RestResponse createPostCall(int port, String service, Map params) throws UnirestException, IOException {
    ObjectMapper mapper = new ObjectMapper();
    String josnBody = mapper.writerWithDefaultPrettyPrinter()
            .writeValueAsString(params);
    HttpResponse<String> response = Unirest.post("http://localhost:" + port + "/api/v1/" + service)
            .header("Content-Type", "application/json")
            .header("cache-control", "no-cache")
            .body(josnBody)
            .asString();
    String jsonResponse = response.getBody();
    return mapper.readValue(jsonResponse, RestResponse.class);
  }
}
