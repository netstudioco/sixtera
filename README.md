# Technical Test Jimmy Gonzalez - Sixtera

El siguiente proyecto se crea como prueba técnica de Jimmy González para Sixtera.

## Paso 1: Configuración de la base de datos

- La base de datos que maneja esta aplicación es MySQL
- Para iniciar el proyecto se debe instalar la base de datos en el motor MySQL y el script se encuentra en el archivo sql/database.sql
- Luego se debe entrar al archivo src/main/resources/application.properties y cambiar el usuario y la clave de MySQL que permite leer y escribir en la base de datos recien creada.

## Paso 2: Iniciar la aplicación

- Para iniciar la aplicación solo se debe inicaiar la JVM con la clase com.jg.sixtera.config.Appliation
- Ya que el proyecto está basado en Spring Boot 2, está clase inicia el servicio web en el puerto 8080

## Servicios

Los servicios que tiene esta aplicación son:

* [GET http://localhost:8080/api/v1/session](http://localhost:8080/api/v1/hello) - Este servicio permite probar que la aplicación se este ejecutando correctamente, debe responder un JSON 

```
{
    "success": true,
    "response": "Hello world"
}
```

* [GET http://localhost:8080/api/v1/session](http://localhost:8080/api/v1/session) - Crea una nueva session para agregar operandos y posteriormente aplicar la operación, cuando el servicio se ejecuta correctamente devuelve un JSON:

```
{
    "success": true,
    "response": 1
}
```

En donde response es el id de la session creada.  Si el servicio no se ejecuta correctamente devuelve un JSON:

```
{
    "success": false,
    "response": "Error message"
}
```

* [POST http://localhost:8080/api/v1/add-operand](http://localhost:8080/api/v1/add-operand) - Agrega un operando a la session, se debe enviar como RequestBody un JSON con la siguiente estructura:
 
```
{
	"sessionId": 1,
	"operandValue": 1
}
```

En donde sessionId es el valor retornado del servicio anterior y el operandValue es el vaor a agregar como operando.  Cuando el servicio se ejecuta correctamente devuelve un JSON:

```
{
    "success": true,
    "response": [
        {
            "operandId": 1,
            "sessionId": 1,
            "operandValue": 1,
            "operationId": null,
            "dateInsert": "2019-05-23T01:40:46.000+0000"
        }
    ]
}
```
Si el servicio no se ejecuta correctamente devuelve un JSON:

```
{
    "success": false,
    "response": "Error message"
}
```

* [POST http://localhost:8080/api/v1/apply-operation](http://localhost:8080/api/v1/apply-operation) - Ejecuta la operacion a los operandos sin una operación, se debe enviar como RequestBody un JSON con la siguiente estructura:
 
```
{
	"sessionId": 1,
	"operCalc": "M"
}
```

En donde sessionId es el valor retornado del segundo servicio y el operCalc la operación a ejecutar; los posibles valores para operCalc son: "A" para suma, "S" para resta, "M" paramultiplicación, "D" para división y "X" para potencia.  Cuando el servicio se ejecuta correctamente devuelve un JSON:

```
{
    "success": true,
    "response": {
        "operationId": 11,
        "sessionId": 1,
        "operCalc": "M",
        "operResult": 1,
        "dateInsert": "2019-05-23T01:41:30.778+0000",
        "operands":  [
            {
                "operandId": 1,
                "sessionId": 1,
                "operandValue": 1,
                "operationId": null,
                "dateInsert": "2019-05-23T01:40:46.000+0000"
            },  {
                "operandId": 2,
                "sessionId": 1,
                "operandValue": 1,
                "operationId": null,
                "dateInsert": "2019-05-23T01:40:46.000+0000"
            }
        ]
    }
}
```
Si el servicio no se ejecuta correctamente devuelve un JSON:

```
{
    "success": false,
    "response": "Error message"
}
```

La aplicación está creando un log en la carpeta logs ubicada en la raíz del proyecto y el nombre del archivo es calculator.log
